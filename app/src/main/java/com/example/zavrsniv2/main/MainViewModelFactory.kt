package com.example.zavrsniv2.main

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.zavrsniv2.camera.CameraViewModel
import com.example.zavrsniv2.database.PhotoDatabaseDao

class MainViewModelFactory(
    private val dataSource: PhotoDatabaseDao,
    private val application: Application): ViewModelProvider.Factory{


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(dataSource,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
