package com.example.zavrsniv2.main


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.example.zavrsniv2.R
import com.example.zavrsniv2.database.PhotoDatabase
import com.example.zavrsniv2.databinding.FragmentMainBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private lateinit var binding: FragmentMainBinding
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var currentPhotoPath: String
    private lateinit var mainViewModel:MainViewModel
    val REQUEST_IMAGE_CAPTURE = 1

    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main,container,false)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.frag_googleMap) as? SupportMapFragment
        mapFragment?.getMapAsync(this)


        val application = requireNotNull(this.activity).application
        val dataSource = PhotoDatabase.getInstace(application).photoDatabaseDao
        val viewModelFactory = MainViewModelFactory(dataSource,application)
        mainViewModel = ViewModelProviders.of(this,viewModelFactory).get(MainViewModel::class.java)

        binding.setLifecycleOwner(this)



        /**
         * Take a photo button, on successful activity result inserts photoObj in database
         * */

        binding.bTakeAPhoto.setOnClickListener {
            dispatchTakePictureIntent()
        }

        /**
         *
         * Observe is photo added to Database, navigate to CameraFrag with photoId
         *
         */

        mainViewModel.navigateToCameraFragment.observe(this,androidx.lifecycle.Observer {
                photo ->
            photo?.let {
                NavHostFragment.findNavController(this).navigate(MainFragmentDirections.actionMainFragmentToCameraFragment(photo.photoId))
            }
        })

        mainViewModel.photoMarkersOptions.observe(this,androidx.lifecycle.Observer {
            it.forEach{
                val exifData = ExifInterface(it.path!!)
                val orgBitmap = exifData.thumbnailBitmap
                val scaledBitmap:Bitmap = Bitmap.createScaledBitmap(orgBitmap!!,(orgBitmap.width*2)/3,(orgBitmap.height*2)/3,false)
                val markerOptions = MarkerOptions().apply {
                    position(LatLng(exifData.latLong!!.component1(),exifData.latLong!!.component2()))
                    icon(BitmapDescriptorFactory.fromBitmap(scaledBitmap))
                }
                map.addMarker(markerOptions)
            }
        })

        return binding.root
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    return
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.example.android.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    /**
     * On finished takePictureIntent: create(photo,lastLocation) and navigate to camerafrag(photoId)
     */

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && currentPhotoPath.isNotEmpty()) {
            mainViewModel.onCreate(currentPhotoPath, lastLocation)
        }
    }


    /**
     *  Functions for maps
     * */

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        map.uiSettings.isMyLocationButtonEnabled = true

        if (map != null) {
            val permission = ActivityCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)

            if (permission == PackageManager.PERMISSION_GRANTED) {
                map?.isMyLocationEnabled = true
            }else {
                requestPermissions( Array(1){Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }

        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) {
                location ->
            if(location != null){
                lastLocation = location
                val currentLatLng = LatLng(location.latitude,location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,8f))
            }
        }

    }

    override fun onMarkerClick(p0: Marker?) = false

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(requireContext(),"Unable to show location - permission required", Toast.LENGTH_LONG).show()
                }else{
                    val mapFragment = childFragmentManager.findFragmentById(R.id.frag_googleMap) as? SupportMapFragment
                    mapFragment?.getMapAsync(this)
                }
            }
        }
    }


}
