package com.example.zavrsniv2.main

import android.app.Application
import android.location.Location
import android.provider.SyncStateContract.Helpers.insert
import android.util.Log
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.zavrsniv2.database.PhotoDatabaseDao
import com.example.zavrsniv2.database.PhotoPath
import kotlinx.coroutines.*

class MainViewModel(
    val database: PhotoDatabaseDao,
    application: Application): AndroidViewModel(application){


    private var viewModelJob = Job()

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var photo = MutableLiveData<PhotoPath?>()

    val photos = database.getAllPhotos()

    val photoMarkersOptions: LiveData<List<PhotoPath>>
    get() = photos


    private val _navigateToCameraFragment = MutableLiveData<PhotoPath>()
    val navigateToCameraFragment: LiveData<PhotoPath>
    get() = _navigateToCameraFragment

    fun doneNavigating(){
        _navigateToCameraFragment.value = null
    }


    fun onCreate(imagePath: String,location: Location){
        uiScope.launch {
            val newPhoto = PhotoPath()
            newPhoto.path = imagePath

            ExifInterface(imagePath).apply {
                setGpsInfo(location)
                saveAttributes()
            }

            insert(newPhoto)
            _navigateToCameraFragment.value = getPhoto()
        }
    }

    private suspend fun getPhoto(): PhotoPath? {
        return withContext(Dispatchers.IO){

            val photo = database.getPhoto()
            photo
        }
    }

    private suspend fun insert(photo: PhotoPath){
        withContext(Dispatchers.IO){
            database.insert(photo)
        }
    }



}