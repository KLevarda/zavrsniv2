package com.example.zavrsniv2.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update


@Dao
interface PhotoDatabaseDao{

    @Insert
    fun insert(path: PhotoPath)

    @Update
    fun update(path: PhotoPath)

    @Query("SELECT * from photo_path_table WHERE photoId = :key")
    fun get(key: Long): PhotoPath

    @Query("DELETE FROM photo_path_table")
    fun clear()

    @Query("SELECT * FROM photo_path_table ORDER BY photoId DESC")
    fun getAllPhotos(): LiveData<List<PhotoPath>>

    @Query("SELECT * FROM photo_path_table ORDER BY photoId DESC LIMIT 1")
    fun getPhoto(): PhotoPath?


}