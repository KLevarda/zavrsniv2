package com.example.zavrsniv2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "photo_path_table")
data class PhotoPath(

    @PrimaryKey(autoGenerate = true)
    var photoId: Long = 0L,

    @ColumnInfo(name="photoPath")
    var path: String? = null,

    @ColumnInfo(name="created_at")
    val createdAt: Long = System.currentTimeMillis()
)