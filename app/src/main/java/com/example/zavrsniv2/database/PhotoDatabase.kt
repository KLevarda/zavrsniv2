package com.example.zavrsniv2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PhotoPath::class],version = 1,exportSchema = false)
abstract class PhotoDatabase: RoomDatabase(){

    abstract val photoDatabaseDao: PhotoDatabaseDao

    companion object{
        @Volatile
        private var INSTANCE: PhotoDatabase? = null

        fun getInstace(context: Context): PhotoDatabase{
            synchronized(this){
                var instance = INSTANCE

                if(instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        PhotoDatabase::class.java,
                        "photo_path_table")
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }

}