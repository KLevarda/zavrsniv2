package com.example.zavrsniv2.camera

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.zavrsniv2.database.PhotoDatabaseDao

class CameraViewModelFactory(
    private val photoKey: Long,
    private val dataSource: PhotoDatabaseDao): ViewModelProvider.Factory{


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(CameraViewModel::class.java)) {
            return CameraViewModel(photoKey,dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
