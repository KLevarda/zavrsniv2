package com.example.zavrsniv2.camera


import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.zavrsniv2.R
import com.example.zavrsniv2.database.PhotoDatabase
import com.example.zavrsniv2.databinding.FragmentCameraBinding
import java.io.File

/**
 *  Take a photo button activates it
 */

// todo: crash onBackButton from locationFragment

class CameraFragment : Fragment() {

    private lateinit var binding: FragmentCameraBinding
    private lateinit var currentPhotoPath: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_camera,container,false)




        val args = CameraFragmentArgs.fromBundle(arguments!!)

        val application = requireNotNull(this.activity).application
        val dataSource = PhotoDatabase.getInstace(application).photoDatabaseDao
        val viewModelFactory = CameraViewModelFactory(args.photoId,dataSource)
        val cameraViewModel = ViewModelProviders.of(this,viewModelFactory).get(CameraViewModel::class.java)

        binding.setLifecycleOwner(this)

        cameraViewModel.temp.observe(this,androidx.lifecycle.Observer {
            if(it != null){
                currentPhotoPath = it.path!!
                setPic()
                galleryAddPic()
                binding.cameraImg.visibility = View.VISIBLE
//                binding.pbLoadingIndicator.visibility = View.GONE
            }
        })
        binding.bAddLocation.setOnClickListener {
            Navigation.findNavController(it).navigate(CameraFragmentDirections.actionCameraFragmentToLocationFragment(args.photoId))
        }

        binding.bSaveButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_cameraFragment_to_mainFragment)
        }


        return  binding.root
    }


    private fun setPic() {
        // Get the dimensions of the View
        val targetW: Int = binding.cameraImg.width
        val targetH: Int = binding.cameraImg.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true
            BitmapFactory.decodeFile(currentPhotoPath, this)
            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = Math.min(photoW / targetW, photoH / targetH)

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
        }
        BitmapFactory.decodeFile(currentPhotoPath,bmOptions)?.also { bitmap ->
            binding.cameraImg.setImageBitmap(bitmap)
        }
    }


    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            requireActivity().sendBroadcast(mediaScanIntent)
        }
    }

}
