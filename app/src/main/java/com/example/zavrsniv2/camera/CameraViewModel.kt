package com.example.zavrsniv2.camera

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zavrsniv2.database.PhotoDatabaseDao
import com.example.zavrsniv2.database.PhotoPath
import kotlinx.coroutines.*

class CameraViewModel(
    private val photoKey: Long = 0L,
    val database: PhotoDatabaseDao): ViewModel(){

    private var viewModelJob = Job()

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var photo = MutableLiveData<PhotoPath?>()

    val temp: LiveData<PhotoPath?>
    get() = photo

    init{
        initalizePhoto()
    }

    private fun initalizePhoto() {
        uiScope.launch {
            photo.value = getPhotoFromDatabase()
        }
    }

    private suspend fun getPhotoFromDatabase(): PhotoPath? {
        return withContext(Dispatchers.IO){
            val photo = database.get(photoKey)
            photo
        }
    }

    fun getPhotoPath():String{

        return photo.value!!.path!!
    }


//    fun getPath():String{
//        var path:String
//
//        uiScope.launch {
//            path = getPhoto()
//        }
//
//        return path
//    }
//
//    private suspend fun getPhoto():String {
//
//        return withContext(Dispatchers.IO){
//            val path = database.get(photoKey)
//            path.path!!
//        }
//
//    }

//    fun onUpdate(){
//        uiScope.launch {
//            val oldPhoto =
//            update(photoKey)
//        }
//    }
//
//    private suspend fun update(photoId: Long){
//        withContext(Dispatchers.IO){
//
//        }
//    }
//

//    val id: Long
//        get() = photoTemp.value!!.photoId
//
//    init {
//        initalizePhoto()
//    }
//
//    private fun initalizePhoto() {
//        uiScope.launch {
//            photoTemp.value = getPhotoFromDatabase()
//        }
//    }
//
//    private suspend fun getPhotoFromDatabase(): PhotoPath? {
//        return withContext(Dispatchers.IO){
//            val photo = database.getPhoto()
//            photo
//        }
//    }

//    fun getPhotoId():Long{
//        uiScope.launch {
//            id = getPhotoIdFromDatabase()
//        }
//        return id
//    }
//
//    private suspend fun getPhotoIdFromDatabase(): Long {
//        return withContext(Dispatchers.IO){
//            val photoId = database.getPhoto()!!.photoId
//            photoId
//        }



}