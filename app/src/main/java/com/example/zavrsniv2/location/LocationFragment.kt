package com.example.zavrsniv2.location


import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.zavrsniv2.R
import com.example.zavrsniv2.database.PhotoDatabase
import com.example.zavrsniv2.databinding.FragmentLocationBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*

class LocationFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    override fun onMarkerClick(p0: Marker?) = false

    private lateinit var binding: FragmentLocationBinding
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


//    val args = GameWonFragmentArgs.fromBundle(arguments!!)
//    Toast.makeText(context, "NumCorrect: ${args.numCorrect}, NumQuestions: ${args.numQuestions}", Toast.LENGTH_LONG).show()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_location,container,false)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.frag_googleMap) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        val args = LocationFragmentArgs.fromBundle(arguments!!)

        val application = requireNotNull(this.activity).application
        val dataSource = PhotoDatabase.getInstace(application).photoDatabaseDao
        val viewModelFactory = LocationViewModelFactory(args.photoId,dataSource)
        val locationViewModel = ViewModelProviders.of(this,viewModelFactory).get(LocationViewModel::class.java)





        Log.d("ADebugTagLocFPhotoID","${args.photoId}")

        binding.bAddLocation.setOnClickListener {
            locationViewModel.onSetLocation(map.cameraPosition.target)
            Navigation.findNavController(it).navigate(LocationFragmentDirections.actionLocationFragmentToCameraFragment(args.photoId))
        }
        return binding.root
    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        map.uiSettings.isMyLocationButtonEnabled = true

        map.setOnCameraMoveListener {
            map.clear()
            binding.imgLocationPinUp.visibility = View.VISIBLE
        }

        map.setOnCameraIdleListener {
            val markerOptions = MarkerOptions().position(map.cameraPosition.target)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_outline_add_location_24px))
            binding.imgLocationPinUp.visibility = View.GONE
            map.addMarker(markerOptions)
        }

        val permission = ActivityCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            map?.isMyLocationEnabled = true
        }else {
            requestPermissions( Array(1){ Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }

        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) {
                location ->
            if(location != null){
                lastLocation = location
                val currentLatLng = LatLng(location.latitude,location.longitude)
//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,16f))
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,16f))
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(requireContext(),"Unable to show location - permission required", Toast.LENGTH_LONG).show()
                }else{
                    val mapFragment = childFragmentManager.findFragmentById(R.id.frag_googleMap) as? SupportMapFragment
                    mapFragment?.getMapAsync(this)
                }
            }
        }

    }

    private fun bitmapDescriptorFromVector(vectorResID: Int):BitmapDescriptor{
        val vectorDrawable: Drawable? = ContextCompat.getDrawable(requireContext(),vectorResID)
        vectorDrawable!!.setBounds(0,0,vectorDrawable.intrinsicWidth,vectorDrawable.intrinsicHeight)
        val bitmap:Bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth,vectorDrawable.intrinsicHeight,Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}
