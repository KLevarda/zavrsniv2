package com.example.zavrsniv2.location



import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.zavrsniv2.database.PhotoDatabaseDao

class LocationViewModelFactory(
    private val photoKey: Long,
    private val dataSource: PhotoDatabaseDao): ViewModelProvider.Factory{


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(LocationViewModel::class.java)) {
            return LocationViewModel(photoKey,dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}

