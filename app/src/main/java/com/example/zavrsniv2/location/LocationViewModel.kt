package com.example.zavrsniv2.location

import android.location.Location
import android.provider.SyncStateContract.Helpers.update
import android.util.Log
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.ViewModel
import com.example.zavrsniv2.database.PhotoDatabaseDao
import com.example.zavrsniv2.database.PhotoPath
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.*


class LocationViewModel(
    private val photoKey: Long = 0L,
    val database: PhotoDatabaseDao): ViewModel(){


    private val viewModelJob = Job()

    override fun onCleared() {
        super.onCleared()

        viewModelJob.cancel()
    }

    private val uiScope =  CoroutineScope(Dispatchers.Main + viewModelJob)

    fun onSetLocation(latLng: LatLng){
        uiScope.launch {
            withContext(Dispatchers.IO){
                val photo = database.get(photoKey)
                ExifInterface(photo.path!!).apply {
                    setLatLong(latLng.latitude,latLng.longitude)
                    saveAttributes()
                }
            }
        }
    }
}