package com.example.zavrsniv2.myGallery

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.exifinterface.media.ExifInterface
import com.example.zavrsniv2.database.PhotoPath

@BindingAdapter("cameraImage")
fun ImageView.setCameraImage(item: PhotoPath?){
    item?.let{
        setImageBitmap(ExifInterface(item.path!!).thumbnailBitmap)
    }
}

@BindingAdapter("photoId")
fun TextView.setPhotoId(item: PhotoPath?){
    item?.let {
        text = item.photoId.toString()
    }
}