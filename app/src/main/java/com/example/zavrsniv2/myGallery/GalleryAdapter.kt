package com.example.zavrsniv2.myGallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.zavrsniv2.database.PhotoPath
import com.example.zavrsniv2.databinding.ListItemPhotoBinding

class GalleryAdapter: ListAdapter<PhotoPath,GalleryAdapter.ViewHolder>(GalleryDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }


    class ViewHolder private constructor(val binding: ListItemPhotoBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(item: PhotoPath){
            binding.photo = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemPhotoBinding.inflate(layoutInflater,parent,false)
                return ViewHolder(binding)
            }
        }

    }

    class GalleryDiffCallback: DiffUtil.ItemCallback<PhotoPath>(){

        override fun areItemsTheSame(oldItem: PhotoPath, newItem: PhotoPath): Boolean {
            return oldItem.photoId == newItem.photoId
        }

        override fun areContentsTheSame(oldItem: PhotoPath, newItem: PhotoPath): Boolean {
            return oldItem == newItem
        }
    }

}