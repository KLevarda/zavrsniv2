package com.example.zavrsniv2.myGallery


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.zavrsniv2.R
import com.example.zavrsniv2.database.PhotoDatabase
import com.example.zavrsniv2.databinding.FragmentMyGalleryBinding
import com.example.zavrsniv2.main.MainViewModel
import com.example.zavrsniv2.main.MainViewModelFactory


class MyGallery : Fragment() {

    private lateinit var binding: FragmentMyGalleryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_my_gallery,container,false)

        val application = requireNotNull(this.activity).application
        val dataSource = PhotoDatabase.getInstace(application).photoDatabaseDao
        val viewModelFactory = MainViewModelFactory(dataSource,application)
        val mainViewModel = ViewModelProviders.of(this,viewModelFactory).get(MainViewModel::class.java)

        val manager = GridLayoutManager(activity,3)
        binding.RVMyGallery.layoutManager = manager

        val adapter = GalleryAdapter()
        binding.RVMyGallery.adapter = adapter

        mainViewModel.photos.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        binding.setLifecycleOwner(this)

        return binding.root
    }


}
